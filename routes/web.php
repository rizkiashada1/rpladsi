<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TablesController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DashboardController;



Route::get('/', [LoginController::class, 'login'])->middleware('guest');
Route::post('/', [LoginController::class, 'authenticate']);

Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register', [RegisterController::class, 'store']);

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/dashboard',[DashboardController::class, 'index']);

Route::get('/tables', [TablesController::class, 'table']);