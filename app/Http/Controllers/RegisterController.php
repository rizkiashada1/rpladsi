<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Models\Register;
use App\Models\User;

class RegisterController extends Controller
{
    public function index()
    {
        return view('navbars.guest.register',[
            'title' => 'Register',
            'active' => 'register'
        ]);
    }
 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:5|max:255'
        ]);
        
        $validatedData['password'] = bcrypt($validatedData['password']);

        //$request->session()->flash('success', 'Registrasi berhasil!!!!');
        
        User::create($validatedData);

        return redirect('/')->with('success', 'Registrasi berhasil!!!!');
    }
}
