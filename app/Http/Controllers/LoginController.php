<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Models\Login;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login()
    {
        return view('navbars.guest.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email'=> 'required|email:dns',
            'password'=>'required'
        ]);

        if(Auth::attempt($credentials)) {
            $request ->session()->regenerate();
            return redirect()->intended('dahsboard');
        }

        return back()->with('loginError', 'Login failed!');
    }
}
